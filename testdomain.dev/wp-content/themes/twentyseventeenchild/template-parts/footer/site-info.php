<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentyseventeen' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'WordPress' ); ?></a>
</div><!-- .site-info -->
<?php
$footer_phone = get_theme_mod( 'footer_phone', 'phone_number' );
$footer_email = get_theme_mod( 'footer_email', 'email_address' );
$footer_button = get_theme_mod( 'footer_button', 'contact_button' );
?>
<?php if ( wp_is_mobile() ) { ?>
	<div class="contact-info-mobile">
	<span class="contact-phone-mobile"><a href="callto:<?php echo $footer_phone; ?>">Call Us</a></span>
	<span class="contact-email-mobile"><a href="mailto:<?php echo $footer_email; ?>">Email Us</a></span>
	<span class="contact-button-mobile"><a class="btn btn-primary btn-lg" href="#myModal1" data-toggle="modal"><?php echo $footer_button; ?></a</span>
</div><!-- .contact-info -->
<?php } else { ?>
<div class="contact-info">
	<span class="contact-phone">Call Us: <?php echo $footer_phone; ?></span>
	<span class="contact-email">Email: <a href="mailto:<?php echo $footer_email; ?>"><?php echo $footer_email; ?></a></span>
	<span class="contact-button"><a class="btn btn-primary btn-lg" href="#myModal1" data-toggle="modal"><?php echo $footer_button; ?></a</span>
</div><!-- .contact-info -->
<?php } ?>
<!-- Modal -->
<div id="myModal1" class="modal fade" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Modal window</h4>
			</div>
			<div class="modal-body">Modal window content goes here...</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button">Submit</button></div>
			</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
