<?php 
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() { 
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
}

// Anatoly Test Tasks
// Creating new options in customizer to add contacts in Footer
/**
* Create Footer contact options
*/
function mytheme_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_section( 
		'mytheme_footer_options', 
		array(
			'title'       => __( 'Footer Settings', 'mytheme' ),
			'priority'    => 100,
			'capability'  => 'edit_theme_options',
			'description' => __('Put Footer contact imformation here.', 'mytheme'), 
		) 
	);
	$wp_customize->add_setting( 'footer_phone'); 
	$wp_customize->add_control(
		'phone_number', 
		array(
			'label'    => __( 'Phone number', 'mytheme' ),
			'section'  => 'mytheme_footer_options',
			'settings' => 'footer_phone',
			'type'     => 'input'
		)
	);
	$wp_customize->add_setting( 'footer_email'); 
	$wp_customize->add_control(
		'email_address', 
		array(
			'label'    => __( 'Email', 'mytheme' ),
			'section'  => 'mytheme_footer_options',
			'settings' => 'footer_email',
			'type'     => 'input'
		)
	);
	$wp_customize->add_setting( 'footer_button'); 
	$wp_customize->add_control(
		'contact_button', 
		array(
			'label'    => __( 'Contact button text', 'mytheme' ),
			'section'  => 'mytheme_footer_options',
			'settings' => 'footer_button',
			'type'     => 'input'
		)
	);
}
add_action( 'customize_register', 'mytheme_register_theme_customizer' );

// Adding my styles/scripts
function themeprefix_bootstrap_modals() {
	wp_register_script ( 'modaljs' , get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '1', true );
	wp_register_style ( 'modalcss' , get_stylesheet_directory_uri() . '/assets/css/bootstrap.css', '' , '', 'all' );
	wp_register_style ( 'responsive' , get_stylesheet_directory_uri() . '/assets/css/responsive.css', '' , '', 'all' );
	
	wp_enqueue_script( 'modaljs' );
	wp_enqueue_style( 'modalcss' );
	wp_enqueue_style( 'responsive' );
}

add_action( 'wp_enqueue_scripts', 'themeprefix_bootstrap_modals');

 /** changing position of elements on a Product page **/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 40 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

function custom_post_type() {

// Set labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Books', 'Post Type General Name', 'mytheme' ),
		'singular_name'       => _x( 'Book', 'Post Type Singular Name', 'mytheme' ),
		'menu_name'           => __( 'Books', 'mytheme' ),
		'parent_item_colon'   => __( 'Parent Book', 'mytheme' ),
		'all_items'           => __( 'All Books', 'mytheme' ),
		'view_item'           => __( 'View Book', 'mytheme' ),
		'add_new_item'        => __( 'Add New Book', 'mytheme' ),
		'add_new'             => __( 'Add New', 'mytheme' ),
		'edit_item'           => __( 'Edit Book', 'mytheme' ),
		'update_item'         => __( 'Update Book', 'mytheme' ),
		'search_items'        => __( 'Search Book', 'mytheme' ),
		'not_found'           => __( 'Not Found', 'mytheme' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'mytheme' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'Books', 'mytheme' ),
		'description'         => __( 'Book news and reviews', 'mytheme' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'genres' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
	// Registering your Custom Post Type
	register_post_type( 'books', $args );

}

add_action( 'init', 'custom_post_type', 0 );

add_action( 'init', 'create_book_tax' );

function create_book_tax() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Genres', 'taxonomy general name', 'mytheme' ),
		'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'mytheme' ),
		'search_items'      => __( 'Search Genres', 'mytheme' ),
		'all_items'         => __( 'All Genres', 'mytheme' ),
		'parent_item'       => __( 'Parent Genre', 'mytheme' ),
		'parent_item_colon' => __( 'Parent Genre:', 'mytheme' ),
		'edit_item'         => __( 'Edit Genre', 'mytheme' ),
		'update_item'       => __( 'Update Genre', 'mytheme' ),
		'add_new_item'      => __( 'Add New Genre', 'mytheme' ),
		'new_item_name'     => __( 'New Genre Name', 'mytheme' ),
		'menu_name'         => __( 'Genre', 'mytheme' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'genre' ),
	);

	register_taxonomy( 'genre', array( 'books' ), $args );
}